import pytest
import allure
from pydantic_schemas.pet import DeletePet, NotFoundPet
from base_classes.response import Response
from enums.pet_enums import PetEnums
from config import API_KEY
from flaky import flaky


@allure.feature('Testing methods separately')
@allure.story('Delete pet method')
@allure.severity(allure.severity_level.CRITICAL)
@pytest.mark.parametrize("fields", PetEnums.PET_TYPES_POSITIVE.value)
@flaky(max_runs=3, min_passes=1)
def test_delete_dog(pet_api, created_new_pet, fields):
    pet_api.assign_allure_name(fields, text="Delete pet with fields:")
    with allure.step(f"Sending the request to delete pet with id =  {created_new_pet}"):
        r = pet_api.delete(path=f"pet/{created_new_pet}", headers={
            "api_key": API_KEY
        })
    with allure.step("Checking the response status and validating schema"):
        response = Response(r)
        response.assert_status_code(PetEnums.SUCCESS_STATUS_CODE.value).validate(DeletePet)

    with allure.step(f"Sending the request to find confirm that pet with id = {created_new_pet} was deleted"):
        del_pet = pet_api.get(path=f"pet/{created_new_pet}", headers={
            "api_key": API_KEY
        })
        deleted_pet = Response(del_pet)
        deleted_pet.assert_status_code(PetEnums.NOT_FOUND_STATUS_CODE.value).validate(NotFoundPet)