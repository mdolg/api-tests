import allure

from pydantic_schemas.pet import UploadPetImage

from base_classes.response import Response

from enums.pet_enums import PetEnums


@allure.feature('Testing methods separately')
@allure.story('Upload pet image method')
@allure.severity(allure.severity_level.TRIVIAL)
def test_upload_image_dog_swagger(pet_api, gen_pet_all_keys):
    pet_api.assign_allure_name(text="Upload pet image in png format")
    with allure.step(f"Sending the request to upload image for pet with id = {gen_pet_all_keys}"):
        r = pet_api.post(path=f"pet/{gen_pet_all_keys}/uploadImage",
                         headers={"accept": "application/json"
                                  }, data={'additionalMetadata': 'additional data'},
                         files={'file': ('Screenshot.png', open('test_data/Screenshot.png', 'rb'), 'image/png')})
    with allure.step("Checking the response status and validating schema"):
        response = Response(r)
        response.assert_status_code(PetEnums.SUCCESS_STATUS_CODE.value).validate(UploadPetImage)
