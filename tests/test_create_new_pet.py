import pytest
import allure

from pydantic_schemas.pet import Pet

from base_classes.response import Response

from enums.pet_enums import PetEnums


@allure.feature('Testing methods separately')
@allure.story('Create new pet method')
@allure.severity(allure.severity_level.BLOCKER)
@pytest.mark.parametrize("fields", PetEnums.PET_TYPES_POSITIVE.value)
def test_create_new_pet(pet_api, gen_empty_pet, fields):
    pet_api.assign_allure_name(fields, text="Pet creating with fields:")
    built_pet = gen_empty_pet.build_pet(fields).build()
    with allure.step("Sending the request to create new pet"):
        r = pet_api.post(path="pet", headers={"accept": "application/json",
                                              "Content-Type": "application/json"
                                              }, json=built_pet
                         )
    with allure.step("Checking the response status and validating schema"):
        response = Response(r)
        response.assert_status_code(PetEnums.SUCCESS_STATUS_CODE.value).validate(Pet)
