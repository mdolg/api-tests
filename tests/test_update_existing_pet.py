import pytest
import allure

from pydantic_schemas.pet import Pet

from base_classes.response import Response

from enums.pet_enums import PetEnums


@allure.feature('Testing methods separately')
@allure.story('Update existing pet method')
@allure.severity(allure.severity_level.BLOCKER)
@pytest.mark.parametrize("fields", PetEnums.PET_TYPES_POSITIVE.value)
def test_update_existing_pet(pet_api, gen_empty_pet, gen_pet_all_keys, fields):
    pet_api.assign_allure_name(fields, text="Pet update with fields:")
    built_pet = gen_empty_pet.build_pet(fields).build()
    with allure.step(f"Sending the request to update pet with id =  {gen_pet_all_keys}"):
        r = pet_api.put(path="pet", headers={"accept": "application/json",
                                             "Content-Type": "application/json"
                                             }, json=built_pet
                        )
    with allure.step("Checking the response status and validating schema"):
        response = Response(r)
        response.assert_status_code(PetEnums.SUCCESS_STATUS_CODE.value).validate(Pet)

    with allure.step(f"Checking that fields {fields} were updated"):
        for i in fields:
            field_name = str.split(i, "_")[1]
            assert response.response_json[field_name] == built_pet[field_name], f"Object field {field_name}" \
                                                                                f"was not correctly updated"
