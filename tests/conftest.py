import pytest
import allure

from generators.pet_generator import PetGenerator
from generators.form_data_generator import FormDataGenerator


@pytest.fixture
def gen_pet_all_keys(pet_api):
    built_pet = PetGenerator().set_id().set_category().set_name().set_photoUrls().set_tags().set_status().build()
    with allure.step("Creating new pet for further testing"):
        response = pet_api.post(path="pet", headers={"accept": "application/json",
                                                     "Content-Type": "application/json"
                                                     }, json=built_pet
                                )
    with allure.step("Deserializing the response and returning pet id for further testing"):
        response = response.json()
        return response["id"]

@pytest.fixture
def gen_empty_pet():
    pet = PetGenerator()
    return pet



@pytest.fixture
def created_new_pet(pet_api, fields):
    built_pet = PetGenerator().build_pet(fields).build()
    with allure.step("Creating new pet for further testing"):
        response = pet_api.post(path="pet", headers={"accept": "application/json",
                                                     "Content-Type": "application/json"
                                                     }, json=built_pet
                                )
    with allure.step("Deserializing the response and returning pet id for further testing"):
        response = response.json()
        return response["id"]

@pytest.fixture
def new_data(fields):
    built_data = FormDataGenerator().build_data(fields).build()
    return built_data
