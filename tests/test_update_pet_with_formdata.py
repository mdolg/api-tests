import pytest
import allure

from pydantic_schemas.pet import UpdatePet

from base_classes.response import Response

from enums.pet_enums import PetEnums

from config import API_KEY


@allure.feature('Testing methods separately')
@allure.story('Update pet with form data method')
@allure.severity(allure.severity_level.MINOR)
@pytest.mark.parametrize("fields", PetEnums.FORM_DATA_TYPES.value)
def test_update_pet_with_formdata(pet_api, gen_pet_all_keys, new_data, fields):
    pet_api.assign_allure_name(fields, text="Update pet with form data with fields:")
    with allure.step(f"Sending the request with form data to update pet with id = {gen_pet_all_keys}"):
        r = pet_api.post(path=f"pet/{gen_pet_all_keys}",
                         headers={"accept": "application/json",
                                  'Content-Type': 'application/x-www-form-urlencoded'
                                  }, data=new_data)
    with allure.step("Checking the response status and validating schema"):
        response = Response(r)
        response.assert_status_code(PetEnums.SUCCESS_STATUS_CODE.value).validate(UpdatePet)

    with allure.step(f"Sending the request to find updated pet with id = {gen_pet_all_keys}"):
        updated_pet = pet_api.get(path=f"pet/{gen_pet_all_keys}", headers={
            "api_key": API_KEY
        })

    with allure.step(f"Checking that fields {fields} were updated"):
        for i in fields:
            field_name = str.split(i, "_")[1]
            assert updated_pet.json()[field_name] == new_data[field_name], f"Object field {field_name}" \
                                                                           f"was not correctly updated"
