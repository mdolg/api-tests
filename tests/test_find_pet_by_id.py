import pytest
import allure

from pydantic_schemas.pet import Pet

from base_classes.response import Response

from enums.pet_enums import PetEnums

from config import API_KEY


@allure.feature('Testing methods separately')
@allure.story('Find pet by id method')
@allure.severity(allure.severity_level.BLOCKER)
@pytest.mark.parametrize("fields", PetEnums.PET_TYPES_POSITIVE.value)
def test_find_pet_by_id(pet_api, created_new_pet, fields):
    pet_api.assign_allure_name(fields, text="Find pet by id with fields:")
    with allure.step(f"Sending the request to find pet with id = {created_new_pet}"):
        r = pet_api.get(path=f"pet/{created_new_pet}", headers={
            "api_key": API_KEY
        })
    with allure.step("Checking the response status and validating schema"):
        response = Response(r)
        response.assert_status_code(PetEnums.SUCCESS_STATUS_CODE.value).validate(Pet)

