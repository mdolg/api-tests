import pytest
import allure

from pydantic_schemas.pet import Pet

from base_classes.response import Response

from enums.pet_enums import PetEnums

from config import API_KEY


@allure.feature('Testing methods separately')
@allure.story('Find dog by status method')
@allure.severity(allure.severity_level.NORMAL)
@pytest.mark.parametrize("status", PetEnums.STATUSES.value)
def test_find_pet_by_status(pet_api, status):
    pet_api.assign_allure_name(text=f"Find all pets by status {status}")
    with allure.step(f"Sending the request to find pets with status = {status} "):
        r = pet_api.get(path="pet/findByStatus", params={"status": {status}}, headers={
            "api_key": API_KEY
        })
    with allure.step("Checking the response status and validating schema"):
        response = Response(r)
        response.assert_status_code(PetEnums.SUCCESS_STATUS_CODE.value).validate(Pet)
    with allure.step(f"Checking that object status is {status}"):
        for pet_object in response.parsed_object:
            assert pet_object.status == status, f"Object has incorrect status = {response.parsed_object.status}"
