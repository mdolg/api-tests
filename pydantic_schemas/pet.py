from pydantic import BaseModel, Field, field_validator, HttpUrl
from pydantic.types import List


class Category(BaseModel):
    id: int = Field(None, description="Category id")
    name: str = Field(None, description="Category name")


class Pet(BaseModel):
    id: int = Field(None, description="Pet id")
    category: Category = Field(None, description="Pet category")
    name: str = Field(..., description="Pet name")
    photoUrls: List[HttpUrl] = Field(..., description="Pet photoUrls")
    tags: list = Field(None, description="Pet tags")
    status: str = Field(None, description="Pet status")


class DeletePet(BaseModel):
    code: int = Field(None, description="Status code")
    type: str = Field(None, description="Type")
    message: str = Field(None, description="Response message")


class UpdatePet(BaseModel):
    code: int = Field(None, description="Status code")
    type: str = Field(None, description="Type")
    message: str = Field(None, description="Response message")


class UploadPetImage(BaseModel):
    code: int = Field(None, description="Status code")
    type: str = Field(None, description="Type")
    message: str = Field(None, description="Response message")

    @field_validator("message")
    def validate_message(cls, message):
        if "additionalMetadata:" not in message:
            raise ValueError("'message key' has wrong formatting ")
        return message


class NotFoundPet(BaseModel):
    code: int = Field(None, description="Status code")
    type: str = Field(None, description="Type")
    message: str = Field(None, description="Response message")

    @field_validator('code')
    def validate_code(cls, code):
        if code != 1:
            raise ValueError("Code must be 1 for NotFoundPet responses")
        return code

    @field_validator('type')
    def validate_type(cls, type):
        if type != "error":
            raise ValueError("Type must be 'error' for NotFoundPet responses")
        return type

    @field_validator('message')
    def validate_message(cls, message):
        if "Pet not found" not in message:
            raise ValueError("'message key' has wrong formatting ")
        return message
