from faker import Faker
import random
from enums.pet_enums import PetEnums


class FormDataGenerator:

    def __init__(self):
        self.result = {}
        self.fake = Faker()

    def set_name(self, name=None):
        if name is None:
            self.result["name"] = self.fake.first_name()
        else:
            self.result["name"] = name
        return self

    def set_status(self, status=None):
        if status is None:
            self.result["status"] = random.choice(PetEnums.STATUSES.value)
        else:
            self.result["status"] = status
        return self

    def build(self):
        return self.result

    def build_data(self, keys):
        for key in keys:
            method_to_call = getattr(self, key)
            method_to_call()
        return self
