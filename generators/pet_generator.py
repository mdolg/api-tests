from faker import Faker
import random
from enums.pet_enums import PetEnums


class PetGenerator:

    def __init__(self):
        self.result = {}
        self.fake = Faker()

    def set_id(self, id=None):
        if id is None:
            self.result["id"] = self.gen_random_id()
        else:
            self.result["id"] = id
        return self

    def set_category(self, category=None):
        if category is None:
            self.result["category"] = {"id": self.gen_random_id(), "name": self.fake.word()}
        else:
            self.result["category"] = category
        return self

    def set_name(self, name=None):
        if name is None:
            self.result["name"] = self.fake.first_name()
        else:
            self.result["name"] = name
        return self

    def set_photoUrls(self, photoUrls=None):
        if photoUrls is None:
            self.result["photoUrls"] = [f"https://example.com/images/{self.fake.word()}.jpg"]
        else:
            self.result["photoUrls"] = photoUrls
        return self

    def set_tags(self, tags=None):
        if tags is None:
            self.result["tags"] = [{
                "id": self.gen_random_id(),
                "name": self.fake.word()
            }]
        else:
            self.result["tags"] = tags
        return self

    def set_status(self, status=None):
        if status is None:
            self.result["status"] = random.choice(PetEnums.STATUSES.value)
        else:
            self.result["status"] = status
        return self

    def build(self):
        return self.result

    def update_inner_value(self, key, value):
        if not isinstance(key, list):
            self.result[key] = value
        else:
            temp = self.result
            for item in key[:-1]:
                if item not in temp.keys():
                    temp[item] = {}
                temp = temp[item]
            temp[key[-1]] = value
        return self

    def build_pet(self, keys):
        for key in keys:
            method_to_call = getattr(self, key)
            method_to_call()
        return self


    def gen_random_id(self):
        return random.randint(1, 1000000)


