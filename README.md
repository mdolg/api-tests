# This project covers testing methods (only positive cases) in 'Everything about your pet' block at https://petstore.swagger.io/
## To start tests:
### Run "docker-compose up -d" command to set up docker containers for allure.
#### Run "pytest -vs --alluredir=allure/allure-results tests/ -n 5" to run tests.
##### To inspect tests results go to 'http://localhost:5252/allure-docker-service-ui/projects/default' and click 'Generate report' "
