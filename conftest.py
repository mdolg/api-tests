import pytest
import requests
import allure

from enums.global_enums import Globals


class ApiClient:
    def __init__(self, base_address):
        self.base_address = base_address

    def post(self, path="/", params=None, data=None, json=None, headers=None, files=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'POST request to: {url}'):
            return requests.post(url=url, params=params, data=data, json=json, headers=headers, files=files)

    def put(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'PUT request to: {url}'):
            return requests.put(url=url, params=params, data=data, json=json, headers=headers)

    def get(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'GET request to: {url}'):
            return requests.get(url=url, params=params, headers=headers)

    def delete(self, path="/", params=None, data=None, headers=None, files=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'Delete request to: {url}'):
            return requests.delete(url=url, params=params, data=data, headers=headers, files=files)

    def assign_allure_name(self, fields=None, text=None):
        if fields is None:
            allure_title = text
            allure.dynamic.title(allure_title)
        else:
            fields_names = [str.split(i, '_')[1] for i in fields]
            allure_title = f"{text} {fields_names}"
            allure.dynamic.title(allure_title)


@pytest.fixture
def pet_api():
    return ApiClient(base_address=Globals.BASE_URL.value)
