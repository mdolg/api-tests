from enums.global_enums import Globals


class Response:

    def __init__(self, response):
        self.response = response
        self.response_json = response.json()
        self.response_status = response.status_code
        self.parsed_object = []

    def validate(self, schema):
        if isinstance(self.response_json, list):
            for item in self.response_json:
                parsed_object = schema.model_validate(item)
                self.parsed_object.append(parsed_object)
        else:
            parsed_object = schema.model_validate(self.response_json)
            self.parsed_object = parsed_object

    def assert_status_code(self, status_code):
        if isinstance(status_code, list):
            assert self.response_status in status_code, f"Expected status code was {status_code}, " \
                                                        f"but we received {self.response_status}"
        else:
            assert self.response_status == status_code, f"Expected status code was {status_code}, " \
                                                        f"but we received {self.response_status}"
        return self

    def get_parsed_object(self):
        return self.parsed_object

    def __str__(self):
        return \
            f"\nStatus code: {self.response_status} \n" \
            f"Requested url: {self.response.url} \n" \
            f"Response body: {self.response_json} \n"
