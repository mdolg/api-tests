from enum import Enum

from base_classes.base_enum import PyEnum


class PetEnums(PyEnum):
    SUCCESS_STATUS_CODE = [200]
    STATUSES = ["available", "pending", "sold"]
    PET_TYPES_POSITIVE = [["set_id", "set_category", "set_name", "set_photoUrls", "set_tags", "set_status"],
                          ["set_category", "set_name", "set_photoUrls", "set_tags", "set_status"],
                          ["set_name", "set_photoUrls", "set_tags", "set_status"],
                          ["set_name", "set_photoUrls", "set_status"],
                          ["set_name", "set_photoUrls"]]
    FORM_DATA_TYPES = [["set_name"], ["set_status"], ["set_name", "set_status"]]
    NOT_FOUND_STATUS_CODE = [404]
