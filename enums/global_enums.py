from enum import Enum


class Globals(Enum):
    BASE_URL = "https://petstore.swagger.io/v2/"

